#ifndef _MEM_DEBUG_
#define _MEM_DEBUG_

#include <stdio.h>

#define DEBUG_FIRST_BYTES 4

void debug_block_info(FILE* f, void const* addr);
void debug_heap_info(FILE* f);

#endif
