#ifndef _MEM_H_
#define _MEM_H_

#include <stdint.h>

#define HEAP_START ((void*)0x04040000)

void* _malloc(size_t query);
void  _free(void* mem);
void* heap_init(size_t initial_size);

#endif
