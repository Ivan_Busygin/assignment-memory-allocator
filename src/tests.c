#define _DEFAULT_SOURCE

#include <stdio.h>
#include <stdint.h>
#include <sys/mman.h>
#include <unistd.h>

#include "../include/mem_internals.h"
#include "../include/mem.h"
#include "../include/mem_debug.h"
#include "../include/util.h"

void first_test() {
  printf("Первый тест.\n\n");

  printf("Инициализируем кучу.");
  heap_init(REGION_MIN_SIZE);
  debug_heap_info(stdout);

  printf("Выделяем блок.");
  void* block = _malloc(5);
  debug_heap_info(stdout);

  printf("Записываем значение через полученный указатель.");
  *((uint8_t*) block) = 5;
  debug_heap_info(stdout);

  printf("Освобождаем блок.");
  _free(block);
  debug_heap_info(stdout);
  printf("\n");
}

void second_test() {
  printf("Второй тест.\n\n");

  printf("Выделяем несколько блоков.");
  void* block1 = _malloc(1);
  void* block2 = _malloc(50);
  void* block3 = _malloc(130);
  debug_heap_info(stdout);

  printf("Освобождаем один блок.");
  _free(block2);
  debug_heap_info(stdout);

  printf("Освобождаем остальные блоки.");
  _free(block1); _free(block3);
  debug_heap_info(stdout);
  printf("\n");
}

void third_test() {
  printf("Третий тест.\n\n");

  printf("Выделяем несколько блоков.");
  void* block1 = _malloc(0);
  void* block2 = _malloc(0);
  void* block3 = _malloc(0);
  void* block4 = _malloc(0);
  debug_heap_info(stdout);

  printf("Освобождаем два блока.");
  _free(block4); _free(block3);
  debug_heap_info(stdout);

  printf("Освобождаем остальные блоки.\n\n\n");
  _free(block1); _free(block2);
}

void fourth_test() {
  printf("Четвёртый тест.\n\n");

  printf("Выделяем блок, занимающий весь регион.");
  void* big_block = _malloc(capacity_from_size((block_size) { REGION_MIN_SIZE }).bytes);
  debug_heap_info(stdout);

  printf("Выделяем ещё один блок.");
  void* scd_block = _malloc(0);
  debug_heap_info(stdout);

  printf("Освобождаем сначала второй, потом первый.");
  _free(scd_block); _free(big_block);
  debug_heap_info(stdout);
  printf("\n");
}

void* block_after(struct block_header const* block);
void* map_pages(void const* addr, size_t length, int additional_flags);
struct block_header* block_get_header(void* contents);

void fifth_test() {
  printf("Пятый тест.\n\n");

  printf("Выделяем блок, занимающий оба региона.");
  void* big_block = _malloc(capacity_from_size((block_size) { REGION_MIN_SIZE * 2 }).bytes);
  debug_heap_info(stdout);

  printf("Выделяем следующую за регионом страницу памяти при помощи mmap.\n");
  void* addr = map_pages(block_after(block_get_header(big_block)), getpagesize(), MAP_FIXED_NOREPLACE);
  if (addr == MAP_FAILED)
    err("Не получилось... Что-то пошло не так.");
  printf("\n");

  printf("Выделяем ещё один блок.");
  void* scd_block = _malloc(0);
  debug_heap_info(stdout);

  printf("Освобождаем сначала второй, потом первый.");
  _free(scd_block); _free(big_block);
  debug_heap_info(stdout);
}
