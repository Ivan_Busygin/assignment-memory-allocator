#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "../include/util.h"

_Noreturn void err(const char* format, ...) {
  va_list args;
  va_start(args, format);
  vfprintf(stderr, format, args);
  va_end(args);
  abort();
}
