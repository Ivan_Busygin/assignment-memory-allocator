#include "../include/mem_debug.h"
#include "../include/mem_internals.h"
#include "../include/mem.h"

void debug_block_info(FILE* f, void const* addr) {
  struct block_header const* header = addr;
  fprintf( f,
           "%14p %10zu %8s   ",
           addr,
           header->capacity.bytes,
           header->is_free ? "free" : "taken"
  );
  for (size_t i = 0; i < DEBUG_FIRST_BYTES && i < header->capacity.bytes; i++)
    fprintf(f, "%hhX ", header->contents[i]);
  fprintf(f, "\n");
}

void debug_heap_info(FILE* f) {
  fprintf(f, "\n%14s %10s %8s %10s\n", "start", "capacity", "status", "contents");
  for (struct block_header const* header = HEAP_START; header; header = header->next)
    debug_block_info(f, header);
  fprintf(f, "\n");
}
