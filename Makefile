CFLAGS = --std=c17 -I src/ -ggdb -Wextra -DDEBUG -Werror -pedantic -Wall
CC = gcc
RM = rm -rf
MKDIR = mkdir -p

SRC_DIR = src
OBJ_DIR = obj
BUILD_DIR = build

FILES = util mem mem_debug tests main

.PHONY: all clean test

all: $(addprefix $(OBJ_DIR)/, $(addsuffix .o, $(FILES))) $(BUILD_DIR)
	$(CC) -o $(BUILD_DIR)/main $(filter %.o, $^)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c $(OBJ_DIR)
	$(CC) -c $(CFLAGS) $< -o $@

$(OBJ_DIR):
	$(MKDIR) $(OBJ_DIR)

$(BUILD_DIR):
	$(MKDIR) $(BUILD_DIR)

clean:
	$(RM) $(OBJ_DIR)
	$(RM) $(BUILD_DIR)

test:
	@+cd tester; make CC=$(CC)

start:
	build/main
